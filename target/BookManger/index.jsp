<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JJPublisher</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="resource/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <style>
        .one{
            position: absolute;
            top:30%;
            background-color: snow;
            border-radius: 6px;
            border: 15px solid snow;
            left:36%;
        }
        .two{
            position: absolute;
            top: 45%;
            left: 47%;
        }

    </style>
</head>
<body>
<img src="resource/img/Poppies.jpg" width="105%" height="110%">
<h1 class="one">Welcome to JJPublisher</h1>
<a href="<c:url value="/registration"/>" class="btn btn-primary btn-lg two" align="center">Try on</a>
</body>
</html>