<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add product</title>
</head>
<body>

<br/>
<br/>

<h1>Add a product</h1>

<c:url var="addAction" value="/products/add"/>

<form:form action="${addAction}" commandName="catalogItem" enctype="multipart/form-data">
    <table>
        <c:if test="${!empty catalogItem.name}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="name">
                    <spring:message text="Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="name"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="description">
                    <spring:message text="Desc"/>
                </form:label>
            </td>
            <td>
                <form:input path="description"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="price">
                    <spring:message text="Price"/>
                </form:label>
            </td>
            <td>
                <form:input path="price"/>
            </td>
        </tr>
        <%--<tr>--%>
            <%--<td>--%>
                <%--<form:label path="product">--%>
                    <%--<spring:message text="Prod"/>--%>
                <%--</form:label>--%>
            <%--</td>--%>
            <%--<td>--%>
                <%--<form:input path="product"/>--%>
            <%--</td>--%>
        <%--</tr>--%>
        <tr>
            <td>
                <input type="file" name="prodImg">
            </td>
        </tr>
        <tr>
            <td>
                <spring-form:select path="product">
                    <spring-form:option value="" label="*** Select Option ***" />
                    <spring-form:options items="${prodEnum}" />
                </spring-form:select>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <c:if test="${!empty catalogItem.name}">
                    <input type="submit"
                           value="<spring:message text="Edit In Catalog"/>"/>
                </c:if>
                <c:if test="${empty catalogItem.name}">
                    <input type="submit"
                           value="<spring:message text="Add To Catalog"/>"/>
                </c:if>
            </td>
        </tr>
    </table>
</form:form>

</body>
</html>
