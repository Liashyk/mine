<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Registration JJPublisher</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>


<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/catalog"/>">JJPublisher</a>
        </div>

        <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-2">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="<c:url value="/catalog"/>"><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge">42</span></a></li>
                <li><a href="<c:url value="/catalog"/>">profile</a></li>
                <li><a href="<c:url value="/login"/>">log in</a></li>
                <li><a href="<c:url value="/catalog"/>">sing up</a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="container-fluid">
    <br>

<c:url var="addAction" value="/registration/try"/>

<form:form action="${addAction}" commandName="registerFields" class="form-horizontal">
        <fieldset>
            <legend> <div class="col-lg-offset-2">Sing up</div></legend>
            <div class="form-group">
                <label class="col-lg-2 control-label">First name</label>
                <div class="col-lg-8">
                    <form:input path="firstName" type="text" class="form-control" placeholder="First name"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Last name</label>
                <div class="col-lg-8">
                    <form:input path="secondName" type="text" class="form-control" placeholder="Last name"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Email</label>
                <div class="col-lg-8">
                    <form:input path="email" type="email" class="form-control" placeholder="Email"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Password</label>
                <div class="col-lg-8">
                    <form:input path="password" type="password" class="form-control" placeholder="Password"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Repeate password</label>
                <div class="col-lg-8">
                    <input type="password" class="form-control" placeholder="Repeate password"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Date of birth</label>
                <div class="col-lg-8">
                    <form:input path="dateOfBirth" type="date" class="form-control"/>
                </div>
            </div>

            <%--<div class="form-group">--%>
                <%--<label class="col-lg-2 control-label">City prefer</label>--%>
                <%--<div class="col-lg-8">--%>
                    <%--<form:select path="cityPrefer">--%>
                        <%--&lt;%&ndash;<form:option value="${emptyCity}" label="--Select city--"/>&ndash;%&gt;--%>
                        <%--<c:forEach items="${cityList}" var="city">--%>
                            <%--<form:option value="${city}">${city.name}</form:option>--%>
                        <%--</c:forEach>--%>
                    <%--</form:select>--%>
                <%--</div>--%>
            <%--</div>--%>
            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-2">
                    <input type="submit" class="btn btn-primary" value="Submit"/>
                    <input type="reset" class="btn btn-default" value="Cancel"/>
                </div>
            </div>
        </fieldset>
</form:form>
</div>
</body>
</html>
