<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Authorization JJPublisher</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <br>
    <c:url var="addAction" value="/login/try"/>
    <form:form action="${addAction}" commandName="loginFields" class="form-horizontal">
        <fieldset>
            <legend><div align="center">Log in</div></legend>
            <div class="form-group">

                <div class="col-md-4 col-md-offset-4">
                    <form:input path="email" type="email" class="form-control" id="inputEmail" placeholder="Email" required="required"/>
                </div>
            </div>
            <div class="form-group">

                <div class="col-md-4 col-md-offset-4">
                    <form:input path="password" type="password" class="form-control" id="inputPassword" placeholder="Password" required="required"/>
                </div>
            </div>
            <div class="form-group">
                <div align="center">
                    <input type="submit" class="btn btn-primary" value="Submit"/>
                    <input type="reset" class="btn btn-default" value="Cancel"/>
                </div>
            </div>
        </fieldset>
    </form:form>
</div>
</body>
</html>
