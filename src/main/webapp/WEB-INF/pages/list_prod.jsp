<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: VL
  Date: 05.05.17
  Time: 11:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of products</title>
</head>
<body>
<h1>List of products</h1>

<c:if test="${!empty listCatalog}">
    <table class="tg">
        <tr>
            <th width="80">ID</th>
            <th width="120">Name</th>
            <th width="120">Description</th>
            <th width="120">Price</th>
            <th width="120">Product</th>
            <th width="60">Edit</th>
            <th width="60">Delete</th>
        </tr>
        <c:forEach items="${listCatalog}" var="catalog">
            <tr>
                <td>${catalog.id}</td>
                <td><a href="/catalogItemData/${catalog.id}" target="_blank">${catalog.name}</a></td>
                <td>${catalog.description}</td>
                <td>${catalog.price/100}${catalog.price%100}</td>
                <td>${catalog.product.name()}</td>
                <td><a href="<c:url value='/editProd/${catalog.id}'/>">Edit</a></td>
                <td><a href="<c:url value='/removeProd/${catalog.id}'/>">Delete</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty listCatalog}">
    <h3>Catalog list is empty</h3>
</c:if>
<a href="<c:url value='/addProd'/>">Add product</a>
</body>
</html>
