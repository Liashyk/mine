<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Books Page</title>

    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9
        }
    </style>
</head>
<body>

<br/>
<br/>

<h1>Catalog list List</h1>

<c:if test="${!empty listCatalog}">
    <table class="tg">
        <tr>
            <th width="80">ID</th>
            <th width="120">Name</th>
            <th width="120">Description</th>
            <th width="120">Price</th>
            <th width="120">Product</th>
            <th width="60">Edit</th>
            <th width="60">Delete</th>
        </tr>
        <c:forEach items="${listCatalog}" var="catalog">
            <tr>
                <td>${catalog.id}</td>
                <td><a href="/catalogItemData/${catalog.id}" target="_blank">${catalog.name}</a></td>
                <td>${catalog.description}</td>
                <td>${catalog.price/100}${catalog.price%100}</td>
                <td>${catalog.product.name()}</td>
                <td><a href="<c:url value='/edit/${catalog.id}'/>">Edit</a></td>
                <td><a href="<c:url value='/remove/${catalog.id}'/>">Delete</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty listCatalog}">
    <h3>Catalog list is empty</h3>
</c:if>


<h1>Add a Book</h1>

<c:url var="addAction" value="/catalog/add"/>

<form:form action="${addAction}" commandName="catalogItem">
    <table>
        <c:if test="${!empty catalogItem.name}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="name">
                    <spring:message text="Name"/>
                </form:label>
            </td>
            <td>
                <form:input path="name"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="description">
                    <spring:message text="Desc"/>
                </form:label>
            </td>
            <td>
                <form:input path="description"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="price">
                    <spring:message text="Price"/>
                </form:label>
            </td>
            <td>
                <form:input path="price"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="product">
                    <spring:message text="Prod"/>
                </form:label>
            </td>
            <td>
                <form:input path="product"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <c:if test="${!empty catalogItem.name}">
                    <input type="submit"
                           value="<spring:message text="Edit In Catalog"/>"/>
                </c:if>
                <c:if test="${empty catalogItem.name}">
                    <input type="submit"
                           value="<spring:message text="Add To Catalog"/>"/>
                </c:if>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
