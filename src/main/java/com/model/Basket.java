package com.model;

import com.model.enums.ItemStatus;

import javax.persistence.*;

@Entity
public class Basket {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn
    private Presence presence;

    @ManyToOne
    @JoinColumn
    private Booking booking;

    @Column
    private int countOfUnits;

    @ManyToOne
    @JoinColumn
    private PhotoGallery picture;

    @Column
    @Enumerated
    private ItemStatus itemStatus;

    public Basket() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Presence getPresence() {
        return presence;
    }

    public void setPresence(Presence presence) {
        this.presence = presence;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public int getCountOfUnits() {
        return countOfUnits;
    }

    public void setCountOfUnits(int countOfUnits) {
        this.countOfUnits = countOfUnits;
    }

    public PhotoGallery getPicture() {
        return picture;
    }

    public void setPicture(PhotoGallery picture) {
        this.picture = picture;
    }

    public ItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(ItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }

    @Override
    public String toString() {
        return "Basket{" +
                "id=" + id +
                ", presence=" + presence +
                ", booking=" + booking +
                ", countOfUnits=" + countOfUnits +
                ", picture=" + picture +
                ", itemStatus=" + itemStatus +
                '}';
    }
}
