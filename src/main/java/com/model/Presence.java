package com.model;

import javax.persistence.*;

@Entity
public class Presence {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn
    private Shop shop;

    @ManyToOne
    @JoinColumn
    private Catalog catalog;

    public Presence() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    @Override
    public String toString() {
        return "Presence{" +
                "id=" + id +
                ", shop=" + shop +
                ", catalog=" + catalog +
                '}';
    }
}
