package com.model.enums;


public enum ItemStatus {
    BOUGHT,
    BOOKED,
    PICKED;
}
