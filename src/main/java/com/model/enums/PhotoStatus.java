package com.model.enums;

public enum PhotoStatus {
    GALLERY_PHOTO,
    USER_PHOTO;
}
