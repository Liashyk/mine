package com.model.enums;

import java.util.Arrays;
import java.util.List;

public enum ProductEnum {

    T_SHORT("T-short", 1),
    POSTER("Poster", 2),
    CARD("Business card", 3),
    CUP("Cup", 4),
    PILLOW("Pillow", 5),
    PHOTO("Photo", 6),
    CALENDAR("Calendar", 7),
    PEN("Pen", 8),
    STICKER("Sticker", 9);

    private final String name;
    private final int id;

    ProductEnum(String name, int id) {
        this.name=name;
        this.id=id;
    }

    public int getId(){
        return id;
    }

    public ProductEnum getProductById(int id){
        return ProductEnum.values()[id-1];
    }

    public String getName() {
        return name;
    }

    public List<ProductEnum> getList(){
        return Arrays.asList(ProductEnum.values());
    }
}
