package com.model;

import com.model.enums.PhotoStatus;

import javax.persistence.*;

@Entity
public class PhotoGallery {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String path;

    @Column
    @Enumerated
    private PhotoStatus photoStatus;

    public PhotoGallery() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public PhotoStatus getPhotoStatus() {
        return photoStatus;
    }

    public void setPhotoStatus(PhotoStatus photoStatus) {
        this.photoStatus = photoStatus;
    }

    @Override
    public String toString() {
        return "PhotoGallery{" +
                "id=" + id +
                ", path='" + path + '\'' +
                ", photoStatus=" + photoStatus +
                '}';
    }
}
