package com.model;

import com.model.enums.ProductEnum;

import javax.persistence.*;

@Entity
public class Catalog {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private int price;

    @Column
    @Enumerated
    private ProductEnum product;

    @ManyToOne
    @JoinColumn
    private PhotoGallery picture;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String bookTitle) {
        this.name = bookTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String bookAuthor) {
        this.description = bookAuthor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ProductEnum getProduct() {
        return product;
    }

    public void setProduct(ProductEnum product) {
        this.product = product;
    }

    public PhotoGallery getPicture() {
        return picture;
    }

    public void setPicture(PhotoGallery picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", product=" + product +
                ", picture=" + picture +
                '}';
    }
}
