package com.controller;

import com.model.Catalog;
import com.model.enums.ProductEnum;
import com.service.interfaces.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Controller
public class AdminController {
    private CatalogService catalogService;

    @Autowired(required = true)
    @Qualifier(value = "catalogService")
    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @RequestMapping(value = "products", method = RequestMethod.GET)
    public String listCatalog(Model model){

        model.addAttribute("catalogItem", new Catalog());
        model.addAttribute("listCatalog", this.catalogService.listCatalog());
        model.addAttribute("prodEnum", ProductEnum.values());

        return "list_prod";
    }

    @RequestMapping(value = "addProd", method = RequestMethod.GET)
    public String addProduct(@ModelAttribute("catalogItem") Catalog catalog){
        return "add_prod";
    }

    @RequestMapping(value = "/products/add", method = RequestMethod.POST)
    public String addToCatalog(@ModelAttribute("catalogItem") Catalog catalog,@RequestParam("prodImg") MultipartFile file){
        if (!file.isEmpty()){
          String imgName = file.getOriginalFilename();
          File temp = new File("/Users/VL/Downloads/Нова папка/Нова папка/mine/src/main/webapp/resource/usr_img/"+imgName);
          try {
              file.transferTo(temp);
           } catch (IllegalStateException e) {
                e.printStackTrace();
           } catch (IOException e) {
                e.printStackTrace();
          }
        }
        if(catalog.getId() == 0){

            this.catalogService.addToCatalog(catalog);
        }else {
            this.catalogService.updateInCatalog(catalog);
        }

        return "redirect:/products";
    }

    @RequestMapping("/removeProd/{id}")
    public String removeFromCatalog(@PathVariable("id") int id){
        this.catalogService.removeFromCatalog(id);

        return "redirect:/products";
    }

    @RequestMapping("editProd/{id}")
    public String editInCatalog(@PathVariable("id") int id, Model model){
        model.addAttribute("catalogItem", this.catalogService.getFromCatalogById(id));
        model.addAttribute("listCatalog", this.catalogService.listCatalog());

        return "redirect:/addProd";
    }

//    @RequestMapping("catalogItemData/{id}")
//    public String catalogItemData(@PathVariable("id") int id, Model model){
//        model.addAttribute("catalogItem", this.catalogService.getFromCatalogById(id));
//
//        return "catalogItemData";
//    }
}
