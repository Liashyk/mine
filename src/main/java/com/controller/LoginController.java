package com.controller;

import com.model.Buyer;
import com.service.interfaces.BuyerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class LoginController {

    private BuyerService buyerService;

    @Autowired(required = true)
    @Qualifier(value = "buyerService")
    public void setBuyerService(BuyerService buyerService) {
        this.buyerService = buyerService;
    }

    @RequestMapping(value="login", method= RequestMethod.GET)
    public String logIn(Model model){
        model.addAttribute("loginFields", new Buyer());

        return "login";
    }

    @RequestMapping(value = "/login/try", method = RequestMethod.POST)
    public String logInTry(@ModelAttribute("loginFields") Buyer buyer){
        if(!buyer.getEmail().isEmpty()&&!buyer.getPassword().isEmpty()){
            if(!buyerService.getBuyer(buyer.getEmail(), buyer.getPassword()).getEmail().isEmpty()){
                return "redirect:/catalog";
            }
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "registration", method = RequestMethod.GET)
    public String registration(Model model){
        model.addAttribute("registerFields", new Buyer());
//        model.addAttribute("cityList", buyerService.getCityList());
//        model.addAttribute("emptyCity", new City());

        return "registration";
    }

    @RequestMapping(value = "/registration/try", method = RequestMethod.POST)
    public String registrationTry(@ModelAttribute("registerFields") Buyer buyer){
        if(!buyer.getEmail().isEmpty()&!buyer.getPassword().isEmpty()){
            buyerService.addBuyer(buyer);

            return "redirect:/catalog";
        }
        return "redirect:/registration";
    }
}
