package com.controller;

import com.model.Catalog;
import com.service.interfaces.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CatalogController {
    private CatalogService catalogService;

    @Autowired(required = true)
    @Qualifier(value = "catalogService")
    public void setCatalogService(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @RequestMapping(value = "catalog", method = RequestMethod.GET)
    public String listCatalog(Model model){
        model.addAttribute("catalogItem", new Catalog());
        model.addAttribute("listCatalog", this.catalogService.listCatalog());

        return "catalog";
    }

    @RequestMapping(value = "/catalog/add", method = RequestMethod.POST)
    public String addToCatalog(@ModelAttribute("catalogItem") Catalog catalog){
        if(catalog.getId() == 0){
            this.catalogService.addToCatalog(catalog);
        }else {
            this.catalogService.updateInCatalog(catalog);
        }

        return "redirect:/catalog";
    }

    @RequestMapping("/remove/{id}")
    public String removeFromCatalog(@PathVariable("id") int id){
        this.catalogService.removeFromCatalog(id);

        return "redirect:/catalog";
    }

    @RequestMapping("edit/{id}")
    public String editInCatalog(@PathVariable("id") int id, Model model){
        model.addAttribute("catalogItem", this.catalogService.getFromCatalogById(id));
        model.addAttribute("listCatalog", this.catalogService.listCatalog());

        return "catalog";
    }

    @RequestMapping("catalogItemData/{id}")
    public String catalogItemData(@PathVariable("id") int id, Model model){
        model.addAttribute("catalogItem", this.catalogService.getFromCatalogById(id));

        return "catalogItemData";
    }
}
