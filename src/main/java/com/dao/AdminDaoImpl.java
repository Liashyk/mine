package com.dao;

import com.dao.interfaces.AdminDao;
import com.model.Admin;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class AdminDaoImpl implements AdminDao {

    private static final Logger logger = LoggerFactory.getLogger(AdminDaoImpl.class);

    private SessionFactory sessionFactory;


    public AdminDaoImpl(){

    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Admin getAdmin(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Admin admin = (Admin) session.load(Admin.class, new Integer(id));
        logger.info("Admin successfully loaded. Admin details: " + admin.toString());
        return admin;
    }

    @Override
    public List<Admin> getAdminList() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Admin> adminList = session.createQuery("from Admin").getResultList();
        for (Admin admin : adminList) {
            logger.info("Admin list: " + admin);
        }
        return adminList;
    }
}
