package com.dao;


import com.dao.interfaces.CityDao;
import com.model.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityDaoImpl implements CityDao {


    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addCity(City city) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(city);
    }

    @Override
    public City getCity(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        City city = (City)session.load(City.class, new Integer(id));

        return city;
    }

    @Override
    public List<City> getCityList() {
        Session session = this.sessionFactory.getCurrentSession();

        List<City> cityList = session.createQuery(" from City").getResultList();
        return cityList;
    }
}
