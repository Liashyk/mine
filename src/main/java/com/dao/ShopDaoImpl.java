package com.dao;

import com.dao.interfaces.ShopDao;
import com.model.City;
import com.model.Shop;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository
public class ShopDaoImpl implements ShopDao{

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addShop(Shop shop) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(shop);
    }

    @Override
    public void updateShop(Shop shop) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(shop);

    }

    @Override
    public Shop getShop(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Shop shop = (Shop) session.load(Shop.class, new Integer(id));

        return shop;
    }

    @Override
    public List<Shop> getShopList() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Shop> shopList = session.createQuery("from Shop ").getResultList();
        return shopList;
    }

    @Override
    public List<Shop> getShopInCity(City city) {
        List<Shop> shopList = new LinkedList<Shop>();
        for(Shop shop: getShopList()){
            if(shop.getCity().equals(city)) {
                shopList.add(shop);
            }
        }
        return shopList;
    }
}
