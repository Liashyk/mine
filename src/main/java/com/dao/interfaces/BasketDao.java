package com.dao.interfaces;


import com.model.Basket;
import com.model.enums.ItemStatus;

import java.util.List;

public interface BasketDao {

    public void addItem(Basket basket);

    public void deleteItem(int id);

    public void updateItem(Basket basket);

    public Basket getItem(int id);

    public List<Basket> getItemsList();

    public void changeStatus(int id, ItemStatus itemStatus);

    public int basketSum();

}
