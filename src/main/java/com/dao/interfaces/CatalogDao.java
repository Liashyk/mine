package com.dao.interfaces;

import com.model.Catalog;

import java.util.List;

public interface CatalogDao {
    public void addToCatalog(Catalog catalog);

    public void updateInCatalog(Catalog catalog);

    public void removeFromCatalog(int id);

    public Catalog getFromCatalogById(int id);

    public List<Catalog> listCatalog();
}
