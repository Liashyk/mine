package com.dao.interfaces;


import com.model.Buyer;

public interface BuyerDao {

    public void addBuyer(Buyer buyer);

    public void updateBuyer(Buyer buyer);

    public Buyer getBuyer(int id);

    public Buyer getBuyer(String login, String path);

}
