package com.dao.interfaces;


import com.model.City;
import com.model.Shop;

import java.util.List;

public interface ShopDao {

    public void addShop(Shop shop);

    public void updateShop(Shop shop);

    public Shop getShop(int id);

    public List<Shop> getShopList();

    public List<Shop> getShopInCity(City city);
}
