package com.dao.interfaces;


import com.model.Booking;
import com.model.Buyer;

import java.util.Date;

public interface BookingDao {
    public void addBooking(Booking booking);

    public void setBookingDate(int id);

    public Booking getBooking(Buyer buyer);

}
