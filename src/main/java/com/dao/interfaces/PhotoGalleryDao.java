package com.dao.interfaces;


import com.model.PhotoGallery;
import com.model.enums.PhotoStatus;

import java.util.List;

public interface PhotoGalleryDao {
    public void addPhoto(PhotoGallery photoGallery);

    public void deletePhoto(int id);

    public PhotoGallery getPhoto(int id);

    public List<PhotoGallery> getPhotoList(PhotoStatus status);
}
