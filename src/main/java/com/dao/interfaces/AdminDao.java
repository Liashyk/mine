package com.dao.interfaces;

import com.model.Admin;

import java.util.List;

public interface AdminDao {
    public Admin getAdmin(int id);

    public List<Admin> getAdminList();
}
