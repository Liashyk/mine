package com.dao.interfaces;

import com.model.City;

import java.util.List;

public interface CityDao {
    public void addCity(City city);

    public City getCity(int id);

    public List<City> getCityList();

}
