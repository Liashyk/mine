package com.dao;

import com.dao.interfaces.PresenceDao;
import com.model.Catalog;
import com.model.Presence;
import com.model.Shop;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository
public class PresenceDaoImpl implements PresenceDao{

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addPresence(Presence presence) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(presence);
    }

    @Override
    public void deletePresence(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Presence presence = (Presence)session.load(Presence.class, new Integer(id));
        session.delete(presence);
    }

    @Override
    public Presence getPresence(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Presence presence = (Presence)session.load(Presence.class, new Integer(id));

        return presence;
    }

    @Override
    public List<Presence> getPresenceList() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Presence> presenceList = session.createQuery("from Presence ").getResultList();
        return presenceList;
    }

    @Override
    public List<Presence> getPresenceListByShop(Shop shop) {
        List<Presence> presenceList = new LinkedList<Presence>();
        for(Presence presence:getPresenceList()){
            if(presence.getShop().equals(shop)){
                presenceList.add(presence);
            }
        }
        return presenceList;
    }

    @Override
    public List<Presence> getPresenceListByCatalog(Catalog catalog) {
        List<Presence> presenceList = new LinkedList<Presence>();
        for(Presence presence:getPresenceList()){
            if(presence.getCatalog().equals(catalog)){
                presenceList.add(presence);
            }
        }
        return presenceList;
    }

    @Override
    public List<Presence> getPresenceListByShopCatalog(Shop shop, Catalog catalog) {
        List<Presence> presenceList = new LinkedList<Presence>();
        for(Presence presence:getPresenceList()){
            if(presence.getShop().equals(shop)&&presence.getCatalog().equals(catalog)){
                presenceList.add(presence);
            }
        }
        return presenceList;
    }
}
