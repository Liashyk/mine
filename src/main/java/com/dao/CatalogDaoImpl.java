package com.dao;

import com.dao.interfaces.CatalogDao;
import com.model.Catalog;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CatalogDaoImpl implements CatalogDao {
    private static final Logger logger = LoggerFactory.getLogger(CatalogDaoImpl.class);

    private SessionFactory sessionFactory;

    public CatalogDaoImpl(){

    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addToCatalog(Catalog catalog) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(catalog);
        logger.info("Catalog successfully saved. Catalog details: " + catalog);
    }

    @Override
    public void updateInCatalog(Catalog catalog) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(catalog);
        logger.info("Catalog successfully update. Catalog details: " + catalog);
    }

    @Override
    public void removeFromCatalog(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Catalog catalog = (Catalog) session.load(Catalog.class, new Integer(id));

        if(catalog !=null){
            session.delete(catalog);
        }
        logger.info("Catalog successfully removed. Catalog details: " + catalog);
    }

    @Override
    public Catalog getFromCatalogById(int id) {
        Session session =this.sessionFactory.getCurrentSession();
        Catalog catalog = (Catalog) session.load(Catalog.class, new Integer(id));
        logger.info("Catalog successfully loaded. Catalog details: " + catalog);

        return catalog;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Catalog> listCatalog() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Catalog> catalogList = session.createQuery(" from Catalog").getResultList();

        for(Catalog catalog : catalogList){
            logger.info("Catalog list: " + catalog);
        }

        return catalogList;
    }
}