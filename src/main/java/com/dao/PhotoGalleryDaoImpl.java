package com.dao;

import com.dao.interfaces.PhotoGalleryDao;
import com.model.PhotoGallery;
import com.model.enums.PhotoStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PhotoGalleryDaoImpl implements PhotoGalleryDao{

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addPhoto(PhotoGallery photoGallery) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(photoGallery);
    }

    @Override
    public void deletePhoto(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        PhotoGallery photoGallery = (PhotoGallery)session.load(PhotoGallery.class, new Integer(id));
        session.delete(photoGallery);
    }

    @Override
    public PhotoGallery getPhoto(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        PhotoGallery photoGallery = (PhotoGallery)session.load(PhotoGallery.class, new Integer(id));

        return photoGallery;
    }

    @Override
    public List<PhotoGallery> getPhotoList(PhotoStatus status) {
        Session session = this.sessionFactory.getCurrentSession();
        List<PhotoGallery> photoGalleryList = session.createQuery("from PhotoGallery ").getResultList();
        return photoGalleryList;
    }
}
