package com.dao;

import com.dao.interfaces.BookingDao;
import com.model.Booking;
import com.model.Buyer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class BookingDaoImpl implements BookingDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addBooking(Booking booking) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(booking);

    }

    @Override
    public void setBookingDate(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Booking booking = (Booking) session.load(Booking.class, new Integer(id));
        booking.setBookingDate(new Date());
        session.persist(booking);
    }

    @Override
    public Booking getBooking(Buyer buyer) {
        Session session =this.sessionFactory.getCurrentSession();
        List<Booking> bookingList = session.createQuery("from Booking").getResultList();
        for(Booking booking: bookingList){
            if(booking.getBookingDate()==null){
                return booking;
            }
        }
        return null;
    }
}
