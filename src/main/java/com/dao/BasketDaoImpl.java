package com.dao;


import com.dao.interfaces.BasketDao;
import com.model.Basket;
import com.model.enums.ItemStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BasketDaoImpl implements BasketDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addItem(Basket basket) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(basket);
    }

    @Override
    public void deleteItem(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Basket basket = (Basket) session.load(Basket.class, new Integer(id));

        if (basket != null) {
            session.delete(basket);
        }

    }

    @Override
    public void updateItem(Basket basket) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(basket);

    }

    @Override
    public Basket getItem(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Basket basket = (Basket) session.load(Basket.class, new Integer(id));

        return basket;
    }

    @Override
    public List<Basket> getItemsList() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Basket> basketList = session.createQuery("from Basket").getResultList();

        return basketList;
    }

    @Override
    public void changeStatus(int id, ItemStatus itemStatus) {
        Session session = this.sessionFactory.getCurrentSession();
        Basket basket = (Basket) session.load(Basket.class, new Integer(id));
        basket.setItemStatus(itemStatus);
        session.update(basket);

    }

    @Override
    public int basketSum() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Basket> basketList = getItemsList();
        int result = 0;
        for(Basket basket: basketList){
            result+=basket.getPresence().getCatalog().getPrice();
        }
        return result;
    }
}
