package com.dao;

import com.dao.interfaces.BuyerDao;
import com.model.Buyer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BuyerDaoImpl implements BuyerDao{


    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addBuyer(Buyer buyer) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(buyer);

    }

    @Override
    public void updateBuyer(Buyer buyer) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(buyer);

    }

    @Override
    public Buyer getBuyer(int id) {

        Session session = this.sessionFactory.getCurrentSession();
        Buyer buyer = (Buyer)session.load(Buyer.class, new Integer(id));

        return buyer;
    }

    @Override
    public Buyer getBuyer(String login, String path) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Buyer> buyerList = session.createQuery("from Buyer ").getResultList();
        Buyer buyer = new Buyer();
        for(Buyer b: buyerList){
            if(b.getEmail().equals(login)&&b.getPassword().equals(path)){
                buyer=b;
            }
        }
        return buyer;
    }
}
