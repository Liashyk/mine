package com.service;

import com.dao.interfaces.BookingDao;
import com.model.Booking;
import com.model.Buyer;
import com.service.interfaces.BookingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookingServiceImpl implements BookingService{

    private BookingDao bookingDao;

    public void setBookingDao(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

    @Override
    @Transactional
    public void addBooking(Booking booking) {
        this.bookingDao.addBooking(booking);
    }

    @Override
    @Transactional
    public void setBookingDate(int id) {
        this.bookingDao.setBookingDate(id);
    }

    @Override
    @Transactional
    public Booking getBooking(Buyer buyer) {
        return this.bookingDao.getBooking(buyer);
    }
}
