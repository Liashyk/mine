package com.service;

import com.dao.interfaces.AdminDao;
import com.model.Admin;
import com.service.interfaces.AdminService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    private AdminDao adminDao;

    public void setAdminDao(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    @Override
    @Transactional
    public Admin getAdmin(int id) {
        return this.adminDao.getAdmin(id);
    }

    @Override
    @Transactional
    public List<Admin> getAdminList() {
        return this.adminDao.getAdminList();
    }
}
