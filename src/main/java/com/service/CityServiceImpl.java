package com.service;

import com.dao.interfaces.CityDao;
import com.model.City;
import com.service.interfaces.CityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CityServiceImpl implements CityService{

    private CityDao cityDao;

    public void setCityDao(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @Override
    @Transactional
    public void addCity(City city) {
        this.cityDao.addCity(city);
    }

    @Override
    @Transactional
    public City getCity(int id) {
        return this.cityDao.getCity(id);
    }

    @Override
    @Transactional
    public List<City> getCityList() {
        return this.cityDao.getCityList();
    }
}
