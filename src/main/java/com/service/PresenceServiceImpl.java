package com.service;

import com.dao.interfaces.PresenceDao;
import com.model.Catalog;
import com.model.Presence;
import com.model.Shop;
import com.service.interfaces.PresenceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PresenceServiceImpl implements PresenceService{

    private PresenceDao presenceDao;

    public void setPresenceDao(PresenceDao presenceDao) {
        this.presenceDao = presenceDao;
    }

    @Override
    @Transactional
    public void addPresence(Presence presence) {
        this.presenceDao.addPresence(presence);
    }

    @Override
    @Transactional
    public void deletePresence(int id) {
        this.presenceDao.deletePresence(id);
    }

    @Override
    @Transactional
    public Presence getPresence(int id) {
        return this.presenceDao.getPresence(id);
    }

    @Override
    @Transactional
    public List<Presence> getPresenceList() {
        return this.presenceDao.getPresenceList();
    }

    @Override
    @Transactional
    public List<Presence> getPresenceListByShop(Shop shop) {
        return this.presenceDao.getPresenceListByShop(shop);
    }

    @Override
    @Transactional
    public List<Presence> getPresenceListByCatalog(Catalog catalog) {
        return this.presenceDao.getPresenceListByCatalog(catalog);
    }

    @Override
    @Transactional
    public List<Presence> getPresenceListByShopCatalog(Shop shop, Catalog catalog) {
        return this.presenceDao.getPresenceListByShopCatalog(shop, catalog);
    }
}
