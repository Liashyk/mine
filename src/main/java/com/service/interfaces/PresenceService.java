package com.service.interfaces;


import com.model.Catalog;
import com.model.Presence;
import com.model.Shop;

import java.util.List;

public interface PresenceService {

    public void addPresence(Presence presence);

    public void deletePresence(int id);

    public Presence getPresence(int id);

    public List<Presence> getPresenceList();

    public List<Presence> getPresenceListByShop(Shop shop);

    public List<Presence> getPresenceListByCatalog(Catalog catalog);

    public List<Presence> getPresenceListByShopCatalog(Shop shop, Catalog catalog);
}
