package com.service.interfaces;


import com.model.Buyer;
import com.model.City;

import java.util.List;

public interface BuyerService {

    public void addBuyer(Buyer buyer);

    public void updateBuyer(Buyer buyer);

    public Buyer getBuyer(int id);

    public Buyer getBuyer(String login, String password);

    public List<City> getCityList();

}
