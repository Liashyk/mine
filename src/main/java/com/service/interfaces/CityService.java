package com.service.interfaces;

import com.model.City;

import java.util.List;

public interface CityService {
    public void addCity(City city);

    public City getCity(int id);

    public List<City> getCityList();

}
