package com.service.interfaces;


import com.model.Admin;

import java.util.List;

public interface AdminService {
    public Admin getAdmin(int id);

    public List<Admin> getAdminList();
}
