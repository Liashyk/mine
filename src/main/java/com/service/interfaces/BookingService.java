package com.service.interfaces;


import com.model.Booking;
import com.model.Buyer;

public interface BookingService {
    public void addBooking(Booking booking);

    public void setBookingDate(int id);

    public Booking getBooking(Buyer buyer);

}
