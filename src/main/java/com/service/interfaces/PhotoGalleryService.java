package com.service.interfaces;


import com.model.PhotoGallery;
import com.model.enums.PhotoStatus;

import java.util.List;

public interface PhotoGalleryService {
    public void addPhoto(PhotoGallery photoGallery);

    public void deletePhoto(int id);

    public PhotoGallery getPhoto(int id);

    public List<PhotoGallery> getPhotoList(PhotoStatus status);
}
