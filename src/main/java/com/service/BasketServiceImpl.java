package com.service;

import com.dao.interfaces.BasketDao;
import com.model.Basket;
import com.model.enums.ItemStatus;
import com.service.interfaces.BasketService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BasketServiceImpl implements BasketService{
    private BasketDao basketDao;

    public void setBasketDao(BasketDao basketDao) {
        this.basketDao = basketDao;
    }

    @Override
    @Transactional
    public void addItem(Basket basket) {
        this.basketDao.addItem(basket);
    }

    @Override
    @Transactional
    public void deleteItem(int id) {
        this.basketDao.deleteItem(id);

    }

    @Override
    @Transactional
    public void updateItem(Basket basket) {
        this.basketDao.updateItem(basket);
    }

    @Override
    @Transactional
    public Basket getItem(int id) {
        return this.basketDao.getItem(id);
    }

    @Override
    @Transactional
    public List<Basket> getItemsList() {
        return this.basketDao.getItemsList();
    }

    @Override
    @Transactional
    public void changeStatus(int id, ItemStatus itemStatus) {
        this.basketDao.changeStatus(id, itemStatus);
    }

    @Override
    @Transactional
    public int basketSum() {
        return this.basketDao.basketSum();
    }
}
