package com.service;

import com.dao.interfaces.BuyerDao;
import com.dao.interfaces.CityDao;
import com.model.Buyer;
import com.model.City;
import com.service.interfaces.BuyerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BuyerServiceImpl implements BuyerService{

    private BuyerDao buyerDao;

    private CityDao cityDao;

    public void setBuyerDao(BuyerDao buyerDao) {
        this.buyerDao = buyerDao;
    }

    public void setCityDao(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @Override
    @Transactional
    public void addBuyer(Buyer buyer) {
        this.buyerDao.addBuyer(buyer);
    }

    @Override
    @Transactional
    public void updateBuyer(Buyer buyer) {
        this.buyerDao.updateBuyer(buyer);
    }

    @Override
    @Transactional
    public Buyer getBuyer(int id) {
        return this.buyerDao.getBuyer(id);
    }

    @Override
    @Transactional
    public Buyer getBuyer(String login, String password) {
        return this.buyerDao.getBuyer(login, password);
    }

    @Override
    @Transactional
    public List<City> getCityList() {
        return cityDao.getCityList();
    }
}
