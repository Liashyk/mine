package com.service;

import com.dao.interfaces.CatalogDao;
import com.model.Catalog;
import com.service.interfaces.CatalogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CatalogServiceImpl implements CatalogService {
    private CatalogDao catalogDao;

    public void setCatalogDao(CatalogDao catalogDao) {
        this.catalogDao = catalogDao;
    }

    @Override
    @Transactional
    public void addToCatalog(Catalog catalog) {
        this.catalogDao.addToCatalog(catalog);
    }

    @Override
    @Transactional
    public void updateInCatalog(Catalog catalog) {
        this.catalogDao.updateInCatalog(catalog);
    }

    @Override
    @Transactional
    public void removeFromCatalog(int id) {
        this.catalogDao.removeFromCatalog(id);
    }

    @Override
    @Transactional
    public Catalog getFromCatalogById(int id) {
        return this.catalogDao.getFromCatalogById(id);
    }

    @Override
    @Transactional
    public List<Catalog> listCatalog() {
        return this.catalogDao.listCatalog();
    }
}
