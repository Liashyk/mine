package com.service;

import com.dao.interfaces.ShopDao;
import com.model.City;
import com.model.Shop;
import com.service.interfaces.ShopService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ShopServiceImpl implements ShopService {

    private ShopDao shopDao;

    public void setShopDao(ShopDao shopDao) {
        this.shopDao = shopDao;
    }

    @Override
    @Transactional
    public void addShop(Shop shop) {
        this.shopDao.addShop(shop);
    }

    @Override
    @Transactional
    public void updateShop(Shop shop) {
        this.shopDao.updateShop(shop);
    }

    @Override
    @Transactional
    public Shop getShop(int id) {
        return this.shopDao.getShop(id);
    }

    @Override
    @Transactional
    public List<Shop> getShopList() {
        return this.shopDao.getShopList();
    }

    @Override
    @Transactional
    public List<Shop> getShopInCity(City city) {
        return this.shopDao.getShopInCity(city);
    }
}
