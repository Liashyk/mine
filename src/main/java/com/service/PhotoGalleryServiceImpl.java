package com.service;

import com.dao.interfaces.PhotoGalleryDao;
import com.model.PhotoGallery;
import com.model.enums.PhotoStatus;
import com.service.interfaces.PhotoGalleryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PhotoGalleryServiceImpl implements PhotoGalleryService{

    private PhotoGalleryDao photoGalleryDao;

    public void setPhotoGalleryDao(PhotoGalleryDao photoGalleryDao) {
        this.photoGalleryDao = photoGalleryDao;
    }

    @Override
    @Transactional
    public void addPhoto(PhotoGallery photoGallery) {
        this.photoGalleryDao.addPhoto(photoGallery);
    }

    @Override
    @Transactional
    public void deletePhoto(int id) {
        this.photoGalleryDao.deletePhoto(id);
    }

    @Override
    @Transactional
    public PhotoGallery getPhoto(int id) {
        return this.photoGalleryDao.getPhoto(id);
    }

    @Override
    @Transactional
    public List<PhotoGallery> getPhotoList(PhotoStatus status) {
        return this.photoGalleryDao.getPhotoList(status);
    }
}
